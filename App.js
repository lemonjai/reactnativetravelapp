import React, { Component } from 'react';
import { Text, View, SafeAreaView, StyleSheet } from 'react-native';
import { createAppContainer } from 'react-navigation';

import Travel from './navigation/Travel';

export default createAppContainer(Travel);