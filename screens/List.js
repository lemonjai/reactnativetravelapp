import React, { Component } from 'react';
import { 
    Text, 
    StyleSheet, 
    View, 
    FlatList, 
    Image, 
    Dimensions, 
    ImageBackground,
    ScrollView,
    Animated 
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome'; 
import Octicons from 'react-native-vector-icons/Octicons';

import * as theme from '../theme';

const {width, height} = Dimensions.get('screen');
const mocks = [
    {
        id: 1,
        user: {
            name: "Lelia Chavez",
            avatar: "https://randomuser.me/api/portraits/women/44.jpg"
        },
        location: "Loutraki, Greece",
        tempature: 34,
        title: "Loutraki, Greece",
        description: "Loutraki - Description",
        rating: 4.7,
        reviews: 3212,
        preview: "https://images.unsplash.com/photo-1458906931852-47d88574a008?auto=format&fit=crop&w=800&q=80",
        images: [
            "https://images.unsplash.com/photo-1458906931852-47d88574a008?auto=format&fit=crop&w=800&q=80",
            "https://images.unsplash.com/photo-1458906931852-47d88574a008?auto=format&fit=crop&w=800&q=80",
            "https://images.unsplash.com/photo-1458906931852-47d88574a008?auto=format&fit=crop&w=800&q=80",
            "https://images.unsplash.com/photo-1458906931852-47d88574a008?auto=format&fit=crop&w=800&q=80"
        ]
    },
    {
        id: 2,
        user: {
            name: "Lelia Chavez",
            avatar: "https://randomuser.me/api/portraits/women/44.jpg"
        },
        location: "Santorini, Greece",
        tempature: 34,
        title: "Santorini, Greece",
        description: "Santorini",
        rating: 4.7,
        reviews: 3212,
        preview: "https://images.unsplash.com/photo-1507501336603-6e31db2be093?auto=format&fit=crop&w=800&q=60",
        images: [
            "https://images.unsplash.com/photo-1507501336603-6e31db2be093?auto=format&fit=crop&w=800&q=80",
            "https://images.unsplash.com/photo-1507501336603-6e31db2be093?auto=format&fit=crop&w=800&q=80",
            "https://images.unsplash.com/photo-1507501336603-6e31db2be093?auto=format&fit=crop&w=800&q=80",
            "https://images.unsplash.com/photo-1507501336603-6e31db2be093?auto=format&fit=crop&w=800&q=80"
        ]
    },
    {
        id: 3,
        user: {
            name: "Lelia Chavez",
            avatar: "https://randomuser.me/api/portraits/women/44.jpg"
        },
        location: "Wallis and Futura",
        tempature: 34,
        title: "Greece 1",
        description: "Greece Description",
        rating: 4.7,
        reviews: 3212,
        preview: "https://images.unsplash.com/photo-1452811906229-cb3dfddb4f29?auto=format&fit=crop&w=800&q=80",
        images: [
            "https://images.unsplash.com/photo-1452811906229-cb3dfddb4f29?auto=format&fit=crop&w=800&q=80",
            "https://images.unsplash.com/photo-1452811906229-cb3dfddb4f29?auto=format&fit=crop&w=800&q=80",
            "https://images.unsplash.com/photo-1452811906229-cb3dfddb4f29?auto=format&fit=crop&w=800&q=80",
            "https://images.unsplash.com/photo-1452811906229-cb3dfddb4f29?auto=format&fit=crop&w=800&q=80"
        ]
    }
]

const styles = StyleSheet.create({
    flex: {
        flex: 1
    },
    column: {
        flexDirection: 'column'
    },
    row: {
        flexDirection: 'row'
    },
    avatar: {
        width: 36, 
        height: 36, 
        borderRadius: 18,
    },
    header: {
        backgroundColor: theme.colors.white,
        paddingHorizontal: 36,
        marginTop: 48,
        paddingBottom: 20,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    articles: {
        
    },
    destinations: {
        flex: 1,
        justifyContent: 'space-between',
    },
    destination: {
        width: width - (36 * 2),
        height: width * 0.6,
        marginHorizontal: 36,
        paddingHorizontal: 36,
        paddingVertical: 24,
        borderRadius: 12,
    },
    destinationInfo: {
        position: 'absolute',
        borderRadius: 12,
        paddingHorizontal: 36,
        paddingVertical: 18,
        bottom: -36,
        right: 36,
        left: 36,
        backgroundColor: theme.colors.white,
    },
    recommended: {
        // padding: 36,
    },
    recommendedList: {
        // paddingHorizontal: 36,
    },
    recommendation: {
        width: (width - (36 * 2)) / 2,
        marginHorizontal: 8,
        backgroundColor: theme.colors.white,
    },
    recommendationImage: {
        width: (width - (36 * 2)) / 2,
        height: (width - (36 * 2)) / 2,
    },
    rating: {
        fontSize: 28,
        color: theme.colors.white,
        fontWeight: 'bold'
    },
    shadow: {
        shadowColor: theme.colors.black,
        shadowOffset: {
            width: 0,
            height: 6
        },
        shadowOpacity: 0.05,
        shadowRadius: 10,
    },
    dots: {
        width: 10,
        height: 10,
        backgroundColor: theme.colors.gray,
        borderWidth: 2.5,
        borderRadius: 5,
        marginHorizontal: 6,
        borderColor: 'transparent'
    },
    activeDot: {
        width: 12.5,
        height: 12.5,
        borderRadius: 6.25,
        borderColor: theme.colors.active,
    }
});

class Articles extends Component {

    scrollX = new Animated.Value(0);

    static navigationOptions = {
        header: (
            <View style={
                [
                    styles.flex, styles.row, styles.header,
                    {justifyContent: 'space-between', alignItems: 'center'}
                ]}
            >
                <View>
                    <Text style={{color: theme.colors.caption}}>Search for place</Text>
                    <Text style={{fontSize: 24}}>Destination</Text>
                </View>
                <View>
                    <Image style={styles.avatar} source={{uri: 'https://randomuser.me/api/portraits/women/32.jpg'}} />
                </View>
            </View>
        )
    }

    renderDots = () => {
        const {destinations} = this.props;
        const dotPosition = Animated.divide(this.scrollX, width)
        return (
            <View style={[
                styles.flex, styles.row, 
                {justifyContent: 'center', alignItems: 'center',marginTop: (36 * 2)}
            ]}>
                {destinations.map((item, index) => {
                    const width = dotPosition.interpolate({
                        inputRange: [index - 1, index, index + 1],
                        outputRange: [0, 2.5, 0],
                        extrapolate: 'clamp'
                    });

                    return (
                        <Animated.View
                            key={`step-${item.id}`} 
                            style={[styles.dots, styles.activeDot, {borderWidth: width}]} 
                        />
                    )
                })}
            </View>
        )
    }

    renderRatings = (rating) => {
        const stars = new Array(5).fill(0);
    }

    renderDestinations = () => {
        return (
            <View style={[styles.column, styles.destinations]}>
                <FlatList 
                    horizontal
                    pagingEnabled
                    scrollEnabled
                    showsHorizontalScrollIndicator={false}
                    decelerationRate={0}
                    scrollEventThrottle={16}
                    snapToAlignment="center"
                    style={{overflow: 'visible'}}
                    data={this.props.destinations}
                    keyExtractor={(item, index) => `${item.id}`}
                    onScroll={Animated.event([{ nativeEvent: {contentOffset: {x: this.scrollX}} }])}
                    renderItem={({item}) => this.renderDestination(item)}
                />
                {this.renderDots()}
            </View>
        )
    }

    renderDestination = (item) => {
        return (
            <ImageBackground
                style={[styles.flex, styles.destination, styles.shadow,]}
                imageStyle={{borderRadius: 12}}
                source={{uri: item.preview}}
            >
                <View style={[styles.row, {justifyContent: 'space-between'}]}>
                    <View styles={{flex: 0}}>
                        <Image source={{uri: item.user.avatar}} style={styles.avatar} />
                    </View>
                    <View styles={[ styles.column, {flex: 2, paddingHorizontal: theme.sizes.padding / 2}]}>
                        <Text style={{color: theme.colors.white, fontWeight: 'bold', }}>{item.user.name}</Text>
                        <Text style={{color: theme.colors.white}}>
                            {item.location}
                        </Text>
                    </View>
                    <View style={[{flex: 0}, {justifyContent: 'center', alignItems: 'flex-end'}]}>
                        <Text style={styles.rating}>{item.rating}</Text>
                    </View>
                </View>
                <View style={[styles.column, styles.destinationInfo, styles.shadow]}>
                    <Text style={{fontSize: theme.sizes.font * 1.25, fontWeight: '500', paddingBottom: 8}}>{item.title}</Text>
                    <Text style={{color: theme.colors.caption}}>{item.description}</Text>
                </View>
            </ImageBackground>
        )
    }

    renderRecommended = () => {
        return (
            <View style={[styles.flex, styles.recommended]}>
                <View style={[
                    styles.row, 
                    {justifyContent: 'space-between', alignItems: 'flex-end', paddingHorizontal: 36, marginVertical: 24}
                ]}>
                    <Text style={{fontSize: theme.sizes.font * 1.4 }}>Recommended</Text>
                    <Text style={{color: theme.colors.caption}}>More</Text>
                </View>
                <View style={[styles.column, styles.recommendedList]}>
                    <FlatList 
                        horizontal
                        pagingEnabled
                        scrollEnabled
                        showsHorizontalScrollIndicator={false}
                        scrollEventThrottle={16}
                        snapToAlignment="center"
                        data={this.props.destinations}
                        keyExtractor={(item, index) => `${item.id}`}
                        renderItem={({item, index}) => this.renderRecommendation(item, index)}
                    />
                </View>
            </View>
        )
    }

    renderRecommendation = (item, index) => {

        const {destinations} = this.props;
        const isLastItem = index === destinations.length - 1;

        return (
            <View style={[ styles.flex, styles.column, styles.recommendation, styles.shadow, 
                index === 0 ? {marginLeft: theme.sizes.margin} : null,
                isLastItem ? {marginRight: theme.sizes.margin / 2} : null
            ]}>
                <View style={[styles.flex, {overflow: 'hidden', borderTopRightRadius: theme.sizes.border, borderTopLeftRadius: theme.sizes.border}]}>
                    <Image style={[styles.recommendationImage]} source={{uri: item.preview}} />
                    <View style={[styles.flex, styles.row, {alignItems: 'center', justifyContent: 'space-evenly', padding: 18, position: 'absolute', top: 0}]}>
                        <Text style={{fontSize: theme.sizes.font * 1.5, color: theme.colors.white}}>{item.tempature}℃</Text>
                        
                    </View>
                </View>
                <View style={[styles.flex, styles.column, {justifyContent: 'space-evenly', padding: 18}]}>
                    <Text style={{fontSize: theme.sizes.font * 1.25, fontWeight: '500', paddingBottom: theme.sizes.padding / 4.5}}>{item.title}</Text>
                    <Text style={{color: theme.colors.caption}}>{item.location}</Text>
                    <Text style={{color: theme.colors.active}}>{item.rating}</Text>
                </View>
            </View>
        )
    }

    render() {
        return (
            <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{paddingBottom: 36,}}>
                {this.renderDestinations()}
                {this.renderRecommended()}
            </ScrollView>
        )
    }
}

Articles.defaultProps = {
    destinations: mocks
};

export default Articles;
